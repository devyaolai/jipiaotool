package com.ylzx.dao;

import com.ylzx.model.Cityairport;
import com.ylzx.model.CityairportExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CityairportMapper {
    long countByExample(CityairportExample example);

    int deleteByExample(CityairportExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Cityairport record);

    int insertSelective(Cityairport record);

    List<Cityairport> selectByExample(CityairportExample example);

    Cityairport selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Cityairport record, @Param("example") CityairportExample example);

    int updateByExample(@Param("record") Cityairport record, @Param("example") CityairportExample example);

    int updateByPrimaryKeySelective(Cityairport record);

    int updateByPrimaryKey(Cityairport record);
}