package com.ylzx.model;

import java.util.Date;

public class Cityairport {
    private Integer id;

    private String cityname;

    private String pinyin;

    private String shortpinyin;

    private String airportcode;

    private String airportname;

    private Double cityindex;

    private String createuser;

    private Date createtime;

    private String modifyuser;

    private Date modifytime;

    private Double isenable;

    private Integer ucode;

    private Integer language;

    private String countrycode;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCityname() {
        return cityname;
    }

    public void setCityname(String cityname) {
        this.cityname = cityname == null ? null : cityname.trim();
    }

    public String getPinyin() {
        return pinyin;
    }

    public void setPinyin(String pinyin) {
        this.pinyin = pinyin == null ? null : pinyin.trim();
    }

    public String getShortpinyin() {
        return shortpinyin;
    }

    public void setShortpinyin(String shortpinyin) {
        this.shortpinyin = shortpinyin == null ? null : shortpinyin.trim();
    }

    public String getAirportcode() {
        return airportcode;
    }

    public void setAirportcode(String airportcode) {
        this.airportcode = airportcode == null ? null : airportcode.trim();
    }

    public String getAirportname() {
        return airportname;
    }

    public void setAirportname(String airportname) {
        this.airportname = airportname == null ? null : airportname.trim();
    }

    public Double getCityindex() {
        return cityindex;
    }

    public void setCityindex(Double cityindex) {
        this.cityindex = cityindex;
    }

    public String getCreateuser() {
        return createuser;
    }

    public void setCreateuser(String createuser) {
        this.createuser = createuser == null ? null : createuser.trim();
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public String getModifyuser() {
        return modifyuser;
    }

    public void setModifyuser(String modifyuser) {
        this.modifyuser = modifyuser == null ? null : modifyuser.trim();
    }

    public Date getModifytime() {
        return modifytime;
    }

    public void setModifytime(Date modifytime) {
        this.modifytime = modifytime;
    }

    public Double getIsenable() {
        return isenable;
    }

    public void setIsenable(Double isenable) {
        this.isenable = isenable;
    }

    public Integer getUcode() {
        return ucode;
    }

    public void setUcode(Integer ucode) {
        this.ucode = ucode;
    }

    public Integer getLanguage() {
        return language;
    }

    public void setLanguage(Integer language) {
        this.language = language;
    }

    public String getCountrycode() {
        return countrycode;
    }

    public void setCountrycode(String countrycode) {
        this.countrycode = countrycode == null ? null : countrycode.trim();
    }
}