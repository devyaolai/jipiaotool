package com.ylzx.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.soecode.lyf.service.BookService;
import com.ylzx.model.Cityairport;

@Controller
@RequestMapping("/jipiao") // url:/模块/资源/{id}/细分 /seckill/list
public class JipiaoController {

	// private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private BookService bookService;

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	private String list(Model model) {
		return "list";// WEB-INF/jsp/"list".jsp
	}

	@RequestMapping(value = "/t", method = RequestMethod.GET)
	private String t(Model model, @RequestParam("old") String old) {
		String oldStr = old;
		StringBuffer newStr = new StringBuffer();
		List<Cityairport> list = bookService.getAllAirport();

		String[] oldR = old.split("\n|\r");
		// 中转航班
		for (String one : oldR) {
			if (!one.contains("1\u002E") && !StringUtils.isEmpty(one)) {
				// 机场码
				for (Cityairport cityairport : list) {
					String airport = cityairport.getAirportcode().trim();
					String name = cityairport.getCityname();
					one = one.replaceAll(airport, name + ",");
				}
				// 1月JAN，二月FEB，三月MAR，四月APR,五月MAY,六月JUN,7月JUL,8月AUG,9月SEP，10月OCT，11月NOV，12月DEC
				one = one.replaceAll("JAN", " 1月 ").replaceAll("FEB", " 2月 ").replaceAll("MAR", " 3月 ")
						.replaceAll("APR", " 4月 ").replaceAll("MAY", " 5月 ").replaceAll("JUN", " 6月 ")
						.replaceAll("JUL", " 7月 ").replaceAll("AUG", " 8月 ").replaceAll("SEP", " 9月 ")
						.replaceAll("OCT", " 10月 ").replaceAll("NOV", " 11月 ").replaceAll("DEC", " 12月 ");

				// 头等舱代表仓位：F A O
				// 公务舱代表舱位：J C D Z I R
				// 经济舱代表舱位：Y B M U H Q V W S T L N K
				one = one.replaceAll(" C ", " 公务舱 ").replaceAll(" J ", " 公务舱 ").replaceAll(" D ", " 公务舱 ")
						.replaceAll(" Z ", " 公务舱 ").replaceAll(" I ", " 公务舱 ").replaceAll(" R ", " 公务舱 ")
						.replaceAll(" Y ", " 经济舱 ").replaceAll(" B ", " 经济舱 ").replaceAll(" M ", " 经济舱 ")
						.replaceAll(" U ", " 经济舱 ").replaceAll(" H ", " 经济舱 ").replaceAll(" Q ", " 经济舱 ")
						.replaceAll(" V ", " 经济舱 ").replaceAll(" W ", " 经济舱 ").replaceAll(" S ", " 经济舱 ")
						.replaceAll(" T ", " 经济舱 ").replaceAll(" L ", " 经济舱 ").replaceAll(" N ", " 经济舱 ")
						.replaceAll(" K ", " 经济舱 ").replaceAll(" F ", " 头等舱 ").replaceAll(" A ", " 头等舱 ")
						.replaceAll(" O ", " 头等舱 ");

				one = one.replaceAll("[0-9]*\\.", "航班:");

				String t[] = one.split("\\s+");

				// 日期
				String temp = t[5] + t[4].replaceAll("[a-zA-Z]+", "") + "号";
				one = "";
				for (int i = 0; i < t.length; i++) {
					if (i == 4) {
						one = one + "   " + temp;
					} else if (i == 5) {

					} else if (i == 0) {
						one = t[i];
					} else if (i == 7) {

					} else if (i == 8) {
						one = one + "  起飞" + t[i].substring(0, 2) + ":" + t[i].substring(2);
					} else if (i == 9) {
						one = one + "  降落" + t[i].substring(0, 2) + ":" + t[i].substring(2);
						;
					} else {
						one = one + "   " + t[i];
					}
				}

			} else if (one.contains("1\u002E")) {
				one = "尊敬的旅客:\n" + one;
				one = one.replaceAll("1\\.", "");
			}
			if (!StringUtils.isEmpty(one)) {
				newStr.append(one + "\n");
			}

		}

		newStr.append("票价:\n票号:\n退改规定:\n预约/改期/订票热线电话\n4006340777[祝你旅途愉快]");

		//
		model.addAttribute("list", list);
		model.addAttribute("newStr", newStr.toString());
		model.addAttribute("oldStr", oldStr);
		// list.jsp + model = ModelAndView
		return "list";// WEB-INF/jsp/"list".jsp
	}

	public static void main(String[] args) {
		String t = " 航班  CA937  公务舱   MO04 9月   首都国际机场LHR HK1   1410 1745     ";
		String[] t2 = t.split("\\s+");
		System.out.println(t2[5] + " " + t2.length);
		System.out.println(t2[5] + t2[4].replaceAll("[a-zA-Z]+", "") + "号");
	}

}
