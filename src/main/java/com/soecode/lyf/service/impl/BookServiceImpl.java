package com.soecode.lyf.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.soecode.lyf.service.BookService;
import com.ylzx.dao.CityairportMapper;
import com.ylzx.model.Cityairport;
import com.ylzx.model.CityairportExample;

@Service
public class BookServiceImpl implements BookService {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	// 注入Service依赖
	@Autowired
	private  CityairportMapper cityairportMapper;


	@Override
	public List<Cityairport> getAllAirport() {
		CityairportExample example = new CityairportExample();
		return cityairportMapper.selectByExample(example);
	}


}
